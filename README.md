# Testing Gitlab CI/CD

Simple `expressjs` based `Hello World` application to test Gitlab CI/CD.

If pipeline is successful, then it should push to `palaasha/expressjs-hello-world`.