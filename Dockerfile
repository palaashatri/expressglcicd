FROM node:current-alpine
RUN mkdir -p /home/node/app
WORKDIR /home/node/app
COPY . /home/node/app/
# USER node
RUN npm install

EXPOSE 8080
CMD [ "node", "app.js" ]